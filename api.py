import requests
import json
import time
import os
import shlex
import urllib.parse as ul
from datetime import datetime
import pytz

url = 'https://www.bibmath.net/ressources/index.php?action=affiche&quoi=mathsup/cours/va.html'

# https://archive.org/developers/tutorial-get-snapshot-wayback.html
def getAvailability(url):
    params = {
        'url': url
    }
    data = requests.get(f'https://archive.org/wayback/available', params).json()
    return data

def getRemoteContent(url):
    return requests.get(url).content

# How to quickly visualize webpage, then choose to archve it and make that once it is archived it is identically to what I visualized? I am not against visualizing a second by another mean but I want it not to wait the archiving.
data = getAvailability(url)
isArchived = data['archived_snapshots'] != {}
if not isArchived:
    print(f'Webpage not archived! Investigate https://codeberg.org/Benjamin_Loison/web-archives/issues')

if isArchived:
    timestamp = data['archived_snapshots']['closest']['timestamp']
    closestUrl = f'https://web.archive.org/web/{timestamp}id_/{url}'
    print(f'Webpage archived on {timestamp} at {closestUrl}!')

    # `digest` https://archive.org/developers/tutorial-compare-snapshot-wayback.html#step-1-get-a-list-of-available-snapshots does not match `curl -s 'https://ens-paris-saclay.fr/sites/default/files/Doctorants/Presentation_generale_CDSN.pdf' | sha1sum` while it should and `base64 -d` does not seem to help.
    # DuckDuckGo and Google results for `"https://web.archive.org/cdx/search/cdx" "digest"` are not interesting.
    isClosestContentIdentical = getRemoteContent(url) == getRemoteContent(closestUrl)
    print('Closest content ' + ('' if isClosestContentIdentical else 'not ') + 'identical')

##

def waitArchiving(url, snapshotRequestTime):
    sleepTime = 1
    while True:
        data = getAvailability(url)
        if data['archived_snapshots'] != {}:
            timestamp = data['archived_snapshots']['closest']['timestamp']
            timestampDatetime = datetime.strptime(timestamp, '%Y%m%d%H%M%S').replace(tzinfo = pytz.utc)
            if snapshotRequestTime < timestampDatetime:
                break
        print(f'Sleeping for {sleepTime} seconds...')
        time.sleep(sleepTime)
        sleepTime *= 2

def webArchive(url, blocking = True):
    snapshotRequestTime = datetime.now().astimezone()

    # What does the following use in terms of https://web.archive.org/save parameters among (the other being not relevant as they require authentication):
    # - `Save outlinks`
    # - `Save error pages (HTTP Status=4xx, 5xx)`, I usually only use this one
    # - `Save screen shot`
    response = requests.get(f'https://web.archive.org/save/{url}')
    # Seems to return the saved URL content.
    print(response.text)
    if blocking:
        waitArchiving(url, snapshotRequestTime)

webArchive(url)

message = f'{url} is now saved in WebArchive!'
os.system(f'notify-send {shlex.quote(message)}')
print(json.dumps(data, indent = 4))

